# A simple web application
A simple computer store web application written with vanilla javascript, HTML and CSS.
## Getting started and usage
1. Clone to a local directory.
2. Open solution in prefered IDE/code editor.
3. Open index.html in a browser. Optionally use a form of live server, e.g VS Code Extension.

## Prerequisites
* None, but VS Code is preferable of course! Always!
