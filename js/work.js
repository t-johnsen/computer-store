
class Work{
    constructor(){
        this.pay = 0;
        this.sallery = 100;
    }

    work(){
        this.pay += this.sallery;
    }

    bank(){
        const tempPay = this.pay;
        this.pay = 0;
        return tempPay;
    }

}
export default Work;