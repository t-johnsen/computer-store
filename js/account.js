
class Account{
    constructor(){
        this.balance = 50;
        this.hasActiveLoan = false;
    }

    getLoan(){
        let desiredAmount = parseInt(prompt("What is the desired amount you would like to loan?"), 10);
        if((desiredAmount > (this.balance *2)) || this.hasActiveLoan || isNaN(desiredAmount)){
            this.alertPopup("No loan. Desired amount too high, or you already have an unused loan...");
            return;
        }  
        this.balance += desiredAmount;
        this.hasActiveLoan = true;
        confirm("Your loan was granted. Congrats!");
    }

    alertPopup(text){
        alert(text);
    }

    buynow(laptopPrice){
        if(!(this.balance >= laptopPrice)) {
            this.alertPopup("You miss the funds dude. You gotta work some more!");
            return;
        }
        this.balance -= laptopPrice;
        this.hasActiveLoan = false;
        confirm("Congrats! You just bought a new computer. See ya🙋🏼‍♀️")
    }

}
export default Account;