const laptops = [
    {
        "name": "Macbook Pro 16'",
        "price": 20000,
        "description": "This is the best laptop in the world!",
        "image": "../images/macbook-pro.jpg",
        "features": [
            "16 inch screen",
            "Best trackpad ever",
            "macOS (which is the best)",
            "Makes you look cool"
        ]
    },
    {
        "name": "HP whatever",
        "price": 300,
        "description": "Don't expect much",
        "image": "../images/hp.jpg",
        "features": [
            "Cracked screen",
            "5 colors display",
            "Windows OS...🙄",
            "Don't let peaople know you own it"
        ]
    },
    {
        "name": "Another laptop",
        "price": 1001,
        "description": "Is this a laptop?",
        "image": "../images/another.jpg",
        "features": [
            "Lame laptop",
            "Has no trackpad",
            "Has to be plugged in at all times"
        ]
    },
    {
        "name": "Another laptop 2.0",
        "price": 2002,
        "description": "Is this a laptop x2? If you buy this, I feel sorry for you...",
        "image": "../images/another.jpg",
        "features": [
            "Windows '95 OS. Yeey",
            "1 core. 1 thread. Don't get to psyced.",
            "Afrikaans keyboard - good luck!"
        ]
    }
]

import Account from './account.js';
import Work from './work.js';

class App {

    constructor() {
        // HTML elements
        this.elLaptops = document.getElementById('laptops');
        this.elWorkBtn = document.getElementById('work');
        this.elBuyNowBtn = document.getElementById('buy-now');
        this.elGetLoanBtn = document.getElementById('get-loan');
        this.elPutMoneyInBankBtn = document.getElementById('bank');
        this.elFeatureSection = document.getElementById('features');
        this.elAccount = document.getElementById('account');

        // Other properties
        this.account = new Account();
        this.work = new Work();
    }

    init() {
        this.fillLaptops();
        this.showFeatureList(this.elLaptops.options[this.elLaptops.selectedIndex]);
        this.showLaptopView(this.elLaptops.options[this.elLaptops.selectedIndex]);
        this.showBalance();
        this.showPay();
        this.bindEvents();
    }

    bindEvents() {

        this.elLaptops.addEventListener('change', () => {
            const opt = this.elLaptops.options[this.elLaptops.selectedIndex];
            this.showFeatureList(opt);
            //show laptop view
            this.showLaptopView(opt);
        });

        this.elGetLoanBtn.addEventListener('click', () => {
            this.account.getLoan();
            this.showBalance();
        });

        this.elPutMoneyInBankBtn.addEventListener('click', () => {
            const totPay = this.work.bank();
            this.account.balance += totPay;
            this.showBalance();
            this.showPay();
        });
        
        this.elWorkBtn.addEventListener('click', () => {
            this.work.work();
            this.showPay();
        });

        this.elBuyNowBtn.addEventListener('click', () => {
            const opt = this.elLaptops.options[this.elLaptops.selectedIndex];
            const optObj = this.getLaptop(opt.value);
            this.account.buynow(optObj.price);
            this.showBalance();
        });

    }

    fillLaptops() {
        let option = document.createElement("option");
        laptops.forEach(laptop => {
            option.text = laptop.name;
            this.elLaptops.appendChild(option.cloneNode(option));
        });
    }

    showFeatureList(option) {
        this.elFeatureSection.innerHTML = "";
        let laptop = laptops.find(lap => lap.name.trim() === option.value.trim());
        const elFeature = document.createElement('p');
        let featureConcat = '';
        laptop.features.forEach(f => {
            featureConcat += f + '\n ';
        });
        elFeature.innerText = featureConcat;
        this.elFeatureSection.appendChild(elFeature);
    }

    showLaptopView(option){
        
        let laptop = laptops.find(lap => lap.name.trim() === option.value.trim());
        // insert laptop image
        const imageBox = document.getElementById('box1');
        document.getElementById('image').src = laptop.image;

        // insert laptop headline and desc
        const laptopText = document.getElementById('box2');
        document.getElementById('laptop-headline').innerText = laptop.name;
        document.getElementById('laptop-desc').innerText = laptop.description;

        //insert price
        document.getElementById('laptop-price').innerText = laptop.price + " kr";
    }

    showBalance() {
        let balanceValue = document.getElementById('balance-value');
        balanceValue.innerHTML = this.account.balance;
    }

    showPay() {
        let balanceValue = document.getElementById('pay-value');
        balanceValue.innerHTML = this.work.pay;
    }

    getLaptop(optName){
        return laptops.find(lap => lap.name.trim() === optName.trim());
    }

}

new App().init();